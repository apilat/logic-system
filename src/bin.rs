use logic_system::{tableaux::Tableaux, Sentence};
use std::{
    io::{stdin, BufRead, BufReader},
    str::FromStr,
};

fn main() {
    let reader = BufReader::new(stdin());
    let mut previous_sentence = None;

    for line in reader.lines() {
        let line = line.expect("failed to read line from stdin");
        let sentence = Sentence::from_str(&line);
        match sentence {
            Ok(sentence) => {
                println!("{}", sentence);
                let same_as_prev = if let Some(previous_sentence) = previous_sentence { previous_sentence == sentence } else { false };
                previous_sentence = Some(sentence.clone());

                let table = sentence.truth_table();
                if table.is_tautology() {
                    println!("Tautological (always true)");
                    let tableaux = Tableaux::prove(sentence)
                        .expect("failed to produce tableaux for tautological statment");
                    let proof = tableaux.natural_deduction();
                    assert!(proof.verify().is_ok(), "generator produced invalid proof");
                    if same_as_prev {
                        println!("Proof:\n{}", proof);
                    } else {
                        println!("Type the statment in again to show a proof");
                    }
                } else if table.is_contradiction() {
                    println!("Contradictory (always false)");
                } else {
                    println!("True when {}", table.get_possible_structure(true).unwrap());
                    println!("False when {}", table.get_possible_structure(true).unwrap());
                }
            }

            Err(err) => {
                println!("{:?}", err);
            }
        }
    }
}
