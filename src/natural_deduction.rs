use crate::{BinaryOp, Sentence, UnaryOp};
use std::{
    cmp::Reverse,
    collections::HashSet,
    fmt::{self, Display, Formatter},
};
use Rule::*;

#[derive(Debug, Clone)]
pub enum Rule {
    Assumption(bool),
    ConjunctionIntro(Box<Proof>, Box<Proof>),
    ConjunctionElim(Box<Proof>),
    ImplicationIntro(Box<Proof>),
    ImplicationElim(Box<Proof>, Box<Proof>),
    DisjunctionIntro(Box<Proof>),
    DisjunctionElim(Box<Proof>, Box<Proof>, Box<Proof>),
    NegationIntro(Box<Proof>, Box<Proof>),
    NegationElim(Box<Proof>, Box<Proof>),
    EquivalenceIntro(Box<Proof>, Box<Proof>),
    EquivalenceElim(Box<Proof>),
}

#[derive(Debug, Clone)]
pub struct Proof {
    stmt: Sentence,
    rule: Rule,
}

impl Proof {
    pub fn new(stmt: Sentence, rule: Rule) -> Proof {
        Proof { stmt, rule }
    }

    fn constituent_proofs(&self) -> Box<[&Proof]> {
        match &self.rule {
            Assumption(_) => Box::new([]),
            ConjunctionIntro(a, b) => Box::new([a, b]),
            ConjunctionElim(a) => Box::new([a]),
            ImplicationIntro(a) => Box::new([a]),
            ImplicationElim(a, b) => Box::new([a, b]),
            DisjunctionIntro(a) => Box::new([a]),
            DisjunctionElim(a, b, c) => Box::new([a, b, c]),
            NegationIntro(a, b) => Box::new([a, b]),
            NegationElim(a, b) => Box::new([a, b]),
            EquivalenceIntro(a, b) => Box::new([a, b]),
            EquivalenceElim(a) => Box::new([a]),
        }
    }

    fn find_assumptions<'a>(&'a self, a: &mut HashSet<&'a Sentence>) {
        if let Assumption(discharged) = self.rule {
            if !discharged {
                a.insert(&self.stmt);
            }
        } else {
            for proof in self.constituent_proofs().iter() {
                proof.find_assumptions(a);
            }
        }
    }

    pub fn assumptions(&self) -> HashSet<&Sentence> {
        let mut a = HashSet::new();
        self.find_assumptions(&mut a);
        a
    }

    pub fn conclusion(&self) -> &Sentence {
        &self.stmt
    }

    fn verify_assuming<'a>(&'a self, ass: &mut HashSet<Discharged<'a>>) -> Result<(), &'a Proof> {
        let valid = match &self.rule {
            Assumption(false) => true,
            Assumption(true) => {
                ass.contains(&Discharged::Direct(&self.stmt))
                    || (if let Sentence::Unary(UnaryOp::Negation, ref s) = &self.stmt {
                        ass.contains(&Discharged::Negation(s))
                    } else {
                        false
                    })
            }

            ConjunctionIntro(ap, bp) => {
                let (a, b) = (ap.conclusion(), bp.conclusion());
                if let Sentence::Binary(BinaryOp::Conjunction, ref c, ref d) = self.stmt {
                    ap.verify_assuming(ass)?;
                    bp.verify_assuming(ass)?;
                    let (c, d) = (&**c, &**d);
                    (a, b) == (c, d) || (a, b) == (d, c)
                } else {
                    false
                }
            }

            ConjunctionElim(ap) => {
                let a = ap.conclusion();
                if let Sentence::Binary(BinaryOp::Conjunction, ref b, ref c) = a {
                    ap.verify_assuming(ass)?;
                    self.stmt == **b || self.stmt == **c
                } else {
                    false
                }
            }

            ImplicationIntro(ap) => {
                let a = ap.conclusion();
                if let Sentence::Binary(BinaryOp::Implication, ref b, ref c) = self.stmt {
                    ass.insert(Discharged::Direct(b));
                    ap.verify_assuming(ass)?;
                    a == &**c
                } else {
                    false
                }
            }

            ImplicationElim(ap, bp) => {
                let (a, b) = (ap.conclusion(), bp.conclusion());
                if let Sentence::Binary(BinaryOp::Implication, ref c, ref d) = b {
                    ap.verify_assuming(ass)?;
                    bp.verify_assuming(ass)?;
                    a == &**c && self.stmt == **d
                } else {
                    false
                }
            }

            DisjunctionIntro(ap) => {
                let a = ap.conclusion();
                if let Sentence::Binary(BinaryOp::Disjunction, ref b, ref c) = self.stmt {
                    ap.verify_assuming(ass)?;
                    a == &**b || a == &**c
                } else {
                    false
                }
            }

            DisjunctionElim(ap, bp, cp) => {
                let (a, b, c) = (ap.conclusion(), bp.conclusion(), cp.conclusion());
                if let Sentence::Binary(BinaryOp::Disjunction, ref d, ref e) = a {
                    let already_assumed_d = !ass.insert(Discharged::Direct(d));
                    bp.verify_assuming(ass)?;
                    if !already_assumed_d {
                        ass.remove(&Discharged::Direct(d));
                    }
                    ass.insert(Discharged::Direct(e));
                    cp.verify_assuming(ass)?;
                    self.stmt == *b && self.stmt == *c
                } else {
                    false
                }
            }

            NegationIntro(ap, bp) => {
                let (a, b) = (ap.conclusion(), bp.conclusion());
                if let (
                    Sentence::Unary(UnaryOp::Negation, ref c),
                    Sentence::Unary(UnaryOp::Negation, ref d),
                ) = (b, &self.stmt)
                {
                    ass.insert(Discharged::Direct(d));
                    ap.verify_assuming(ass)?;
                    bp.verify_assuming(ass)?;
                    a == &**c
                } else {
                    false
                }
            }

            NegationElim(ap, bp) => {
                let (a, b) = (ap.conclusion(), bp.conclusion());
                if let Sentence::Unary(UnaryOp::Negation, ref c) = b {
                    ass.insert(Discharged::Negation(&self.stmt));
                    ap.verify_assuming(ass)?;
                    bp.verify_assuming(ass)?;
                    a == &**c
                } else {
                    false
                }
            }

            EquivalenceIntro(ap, bp) => {
                if let (
                    Sentence::Binary(BinaryOp::Implication, ref c, ref d),
                    Sentence::Binary(BinaryOp::Implication, ref e, ref f),
                    Sentence::Binary(BinaryOp::Equivalence, ref g, ref h),
                ) = (ap.conclusion(), bp.conclusion(), &self.stmt)
                {
                    ap.verify_assuming(ass)?;
                    bp.verify_assuming(ass)?;
                    // c -> d, e -> f |= g <> h
                    (c, d) == (f, e) && ((c, d) == (g, h) || (c, d) == (h, g))
                } else {
                    false
                }
            }

            EquivalenceElim(ap) => {
                if let (
                    Sentence::Binary(BinaryOp::Equivalence, ref c, ref d),
                    Sentence::Binary(BinaryOp::Implication, ref e, ref f),
                ) = (ap.conclusion(), &self.stmt)
                {
                    // c <> d |= e -> f
                    ap.verify_assuming(ass)?;
                    (c, d) == (e, f) || (c, d) == (f, e)
                } else {
                    false
                }
            }
        };

        if valid {
            Ok(())
        } else {
            Err(self)
        }
    }

    pub fn verify(&self) -> Result<(), &Proof> {
        self.verify_assuming(&mut HashSet::new())
    }
}

impl Display for Proof {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        #[derive(Debug, Clone, Copy)]
        struct Range {
            min: usize,
            max: usize,
        }
        impl Range {
            fn contains(&self, x: usize) -> bool {
                self.min <= x && x <= self.max
            }
        }

        #[derive(Debug)]
        struct Position {
            x: usize,
            y: usize,
            line: Option<Range>,
            s: String,
        }

        fn assign_positions<'a>(
            proof: &'a Proof,
            positions: &mut Vec<Position>,
            column: &mut usize,
            depth: usize,
        ) -> usize {
            let stmt_string = if let Assumption(true) = proof.rule {
                format!("[{}]", proof.stmt)
            } else {
                format!("{}", proof.stmt)
            };

            let proofs = proof.constituent_proofs();
            if proofs.is_empty() {
                positions.push(Position {
                    x: *column,
                    y: depth,
                    line: None,
                    s: stmt_string,
                });
                return *column;
            }

            let (mut x, mut left, mut right) = (None, None, None);
            for (i, proof) in proofs.iter().enumerate() {
                if i > 0 {
                    *column += 1;
                }
                if i == proofs.len() / 2 && proofs.len() % 2 == 0 {
                    x = Some(*column);
                    *column += 1;
                }
                let v = assign_positions(proof, positions, column, depth + 1);
                if i == proofs.len() / 2 && proofs.len() % 2 == 1 {
                    x = Some(v);
                }
                left = Some(left.unwrap_or(usize::MAX).min(v));
                right = Some(right.unwrap_or(usize::MIN).max(v));
            }

            let (x, left, right) = (x.unwrap(), left.unwrap(), right.unwrap());
            positions.push(Position {
                x,
                y: depth,
                line: Some(Range {
                    min: left.min(x),
                    max: right.max(x),
                }),
                s: stmt_string,
            });
            x
        }

        let mut positions = Vec::new();
        assign_positions(self, &mut positions, &mut 0, 0);
        positions.sort_by_key(|Position { x, y, .. }| (Reverse(*y), *x));

        let cols = positions.iter().map(|Position { x, .. }| *x).max().unwrap() + 1;
        let rows = positions.iter().map(|Position { y, .. }| *y).max().unwrap() + 1;
        let mut col_width = vec![0; cols];
        for pos in positions.iter() {
            // Technically the number of characters is not necessarily the width but this covers
            // the majority of cases.
            col_width[pos.x] = col_width[pos.x].max(pos.s.chars().count());
        }

        let mut i = 0;
        for y in (0..rows).rev() {
            if y < rows - 1 {
                let mut j = i;
                let mut on = false;
                for (x, &width) in col_width.iter().enumerate() {
                    if x > 0 {
                        write!(f, "{}", if on { "--" } else { "  " })?;
                    }
                    while j < positions.len() && positions[j].line.is_none() {
                        j += 1;
                    }
                    if j < positions.len()
                        && positions[j].line.unwrap().contains(x)
                        && positions[j].y == y
                    {
                        if x == positions[j].line.unwrap().max {
                            on = false;
                            j += 1;
                        } else {
                            on = true;
                        }
                        write!(f, "{0:-^1$}", "", width)?;
                    } else {
                        write!(f, "{0:^1$}", "", width)?;
                    };
                }
                writeln!(f)?;
            }

            for (x, &width) in col_width.iter().enumerate() {
                if x > 0 {
                    write!(f, "  ")?;
                }
                if i < positions.len() && positions[i].x == x && positions[i].y == y {
                    write!(f, "{0:^1$}", &positions[i].s, width)?;
                    i += 1;
                } else {
                    write!(f, "{0:^1$}", "", width)?;
                };
            }
            writeln!(f)?;
        }
        assert_eq!(i, positions.len());

        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
// We need to use this enum to represent discharged assumptions because of lifetime issues when
// generating a negated assumption for NegationElim.
enum Discharged<'a> {
    Direct(&'a Sentence),
    Negation(&'a Sentence),
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn lem() {
        let p = Sentence::Letter(('P', 0));
        let np = Sentence::Unary(UnaryOp::Negation, Box::new(p.clone()));
        let lem = Sentence::Binary(
            BinaryOp::Disjunction,
            Box::new(p.clone()),
            Box::new(np.clone()),
        );
        let nlem = Sentence::Unary(UnaryOp::Negation, Box::new(lem.clone()));

        let proof = Proof {
            stmt: lem.clone(),
            rule: NegationElim(
                Box::new(Proof {
                    stmt: lem.clone(),
                    rule: DisjunctionIntro(Box::new(Proof {
                        stmt: np.clone(),
                        rule: NegationIntro(
                            Box::new(Proof {
                                stmt: lem.clone(),
                                rule: DisjunctionIntro(Box::new(Proof {
                                    stmt: p.clone(),
                                    rule: Assumption(true),
                                })),
                            }),
                            Box::new(Proof {
                                stmt: nlem.clone(),
                                rule: Assumption(true),
                            }),
                        ),
                    })),
                }),
                Box::new(Proof {
                    stmt: nlem.clone(),
                    rule: Assumption(true),
                }),
            ),
        };

        assert_eq!(proof.conclusion(), &lem);
        assert_eq!(proof.assumptions(), HashSet::new());
        assert!(proof.verify().is_ok());
        assert_eq!(
            proof.to_string(),
            "  \
  [P]                                                 
--------                                              
(P ∨ ¬P)            [¬(P ∨ ¬P)]                       
-------------------------------                       
             ¬P                                       
          --------                                    
          (P ∨ ¬P)                         [¬(P ∨ ¬P)]
          --------------------------------------------
                                 (P ∨ ¬P)             
"
        );
    }

    #[test]
    fn implication_chain() {
        let p = Sentence::Letter(('P', 0));
        let q = Sentence::Letter(('Q', 0));
        let r = Sentence::Letter(('R', 0));
        let ptq = Sentence::Binary(
            BinaryOp::Implication,
            Box::new(p.clone()),
            Box::new(q.clone()),
        );
        let qtr = Sentence::Binary(
            BinaryOp::Implication,
            Box::new(q.clone()),
            Box::new(r.clone()),
        );
        let ptr = Sentence::Binary(
            BinaryOp::Implication,
            Box::new(p.clone()),
            Box::new(r.clone()),
        );

        let proof = Proof {
            stmt: ptr.clone(),
            rule: ImplicationIntro(Box::new(Proof {
                stmt: r.clone(),
                rule: ImplicationElim(
                    Box::new(Proof {
                        stmt: q.clone(),
                        rule: ImplicationElim(
                            Box::new(Proof {
                                stmt: p.clone(),
                                rule: Assumption(true),
                            }),
                            Box::new(Proof {
                                stmt: ptq.clone(),
                                rule: Assumption(false),
                            }),
                        ),
                    }),
                    Box::new(Proof {
                        stmt: qtr.clone(),
                        rule: Assumption(false),
                    }),
                ),
            })),
        };

        assert_eq!(proof.conclusion(), &ptr);
        assert_eq!(proof.assumptions(), vec![&ptq, &qtr].into_iter().collect());
        assert!(proof.verify().is_ok());
        assert_eq!(
            proof.to_string(),
            "\
[P]     (P → Q)                  
---------------                  
     Q                    (Q → R)
     ----------------------------
                    R            
                 -------         
                 (P → R)         
"
        );
    }

    #[test]
    fn implication_chain_incorrect() {
        let p = Sentence::Letter(('P', 0));
        let q = Sentence::Letter(('Q', 0));
        let r = Sentence::Letter(('R', 0));
        let ptq = Sentence::Binary(
            BinaryOp::Implication,
            Box::new(p.clone()),
            Box::new(q.clone()),
        );
        let qtr = Sentence::Binary(
            BinaryOp::Implication,
            Box::new(q.clone()),
            Box::new(r.clone()),
        );
        let ptr = Sentence::Binary(
            BinaryOp::Implication,
            Box::new(p.clone()),
            Box::new(r.clone()),
        );

        let proof = Proof {
            stmt: ptr.clone(),
            rule: ImplicationIntro(Box::new(Proof {
                stmt: r.clone(),
                rule: ImplicationElim(
                    Box::new(Proof {
                        stmt: q.clone(),
                        rule: ImplicationElim(
                            Box::new(Proof {
                                stmt: p.clone(),
                                rule: Assumption(true),
                            }),
                            Box::new(Proof {
                                stmt: ptq.clone(),
                                rule: Assumption(false),
                            }),
                        ),
                    }),
                    Box::new(Proof {
                        stmt: qtr.clone(),
                        rule: Assumption(true),
                    }),
                ),
            })),
        };

        assert_eq!(proof.conclusion(), &ptr);
        assert_eq!(proof.assumptions(), vec![&ptq].into_iter().collect());
        assert!(proof.verify().is_err());
        assert_eq!(
            proof.to_string(),
            "\
[P]     (P → Q)                    
---------------                    
     Q                    [(Q → R)]
     ------------------------------
                    R              
                 -------           
                 (P → R)           
"
        );
    }

    #[test]
    fn neg_implication() {
        let p = Sentence::Letter(('P', 0));
        let q = Sentence::Letter(('Q', 0));
        let np = Sentence::Unary(UnaryOp::Negation, Box::new(p.clone()));
        let nq = Sentence::Unary(UnaryOp::Negation, Box::new(q.clone()));
        let ptq = Sentence::Binary(
            BinaryOp::Implication,
            Box::new(p.clone()),
            Box::new(q.clone()),
        );
        let nptq = Sentence::Unary(UnaryOp::Negation, Box::new(ptq.clone()));
        let panq = Sentence::Binary(
            BinaryOp::Conjunction,
            Box::new(p.clone()),
            Box::new(nq.clone()),
        );

        let proof = Proof {
            stmt: panq.clone(),
            rule: ConjunctionIntro(
                Box::new(Proof {
                    stmt: p.clone(),
                    rule: NegationElim(
                        Box::new(Proof {
                            stmt: ptq.clone(),
                            rule: ImplicationIntro(Box::new(Proof {
                                stmt: q.clone(),
                                rule: NegationElim(
                                    Box::new(Proof {
                                        stmt: p.clone(),
                                        rule: Assumption(true),
                                    }),
                                    Box::new(Proof {
                                        stmt: np.clone(),
                                        rule: Assumption(true),
                                    }),
                                ),
                            })),
                        }),
                        Box::new(Proof {
                            stmt: nptq.clone(),
                            rule: Assumption(false),
                        }),
                    ),
                }),
                Box::new(Proof {
                    stmt: nq.clone(),
                    rule: NegationIntro(
                        Box::new(Proof {
                            stmt: ptq.clone(),
                            rule: ImplicationIntro(Box::new(Proof {
                                stmt: q.clone(),
                                rule: Assumption(true),
                            })),
                        }),
                        Box::new(Proof {
                            stmt: nptq.clone(),
                            rule: Assumption(false),
                        }),
                    ),
                }),
            ),
        };

        assert_eq!(proof.conclusion(), &panq);
        assert_eq!(proof.assumptions(), vec![&nptq].into_iter().collect());
        assert!(proof.verify().is_ok());
        assert_eq!(
            proof.to_string(),
            "\
[P]           [¬P]                                              
------------------                                              
        Q                                    [Q]                
     -------                               -------              
     (P → Q)           ¬(P → Q)            (P → Q)      ¬(P → Q)
     --------------------------            ---------------------
                    P                               ¬Q          
                    ----------------------------------          
                                 (P ∧ ¬Q)                       
"
        );
    }
}
