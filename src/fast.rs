use crate::{
    natural_deduction::{Proof, Rule},
    BinaryOp, Sentence, UnaryOp,
};

pub trait Makes<T>
where
    Self: Sized,
{
    fn make(self) -> T;
    fn make_box(self) -> Box<T> {
        Box::new(self.make())
    }
}

impl<T> Makes<T> for T {
    fn make(self) -> T {
        self
    }
}

impl<T: Clone> Makes<T> for &'_ T {
    fn make(self) -> T {
        self.clone()
    }
}

impl<T> Makes<T> for Box<T> {
    fn make(self) -> T {
        *self
    }
    fn make_box(self) -> Box<T> {
        self
    }
}

impl<T: Clone> Makes<T> for &'_ Box<T> {
    fn make(self) -> T {
        (&**self).clone()
    }
    fn make_box(self) -> Box<T> {
        self.clone()
    }
}

type S = Box<Sentence>;
type P = Box<Proof>;

pub fn letter0(c: char) -> S {
    letter(c, 0)
}
pub fn letter(c: char, i: u32) -> S {
    Box::new(Sentence::Letter((c, i)))
}

fn unary(op: UnaryOp, s: impl Makes<Sentence>) -> S {
    Box::new(Sentence::Unary(op, s.make_box()))
}
pub fn neg(s: impl Makes<Sentence>) -> S {
    unary(UnaryOp::Negation, s)
}

fn binary(op: BinaryOp, s: impl Makes<Sentence>, p: impl Makes<Sentence>) -> S {
    Box::new(Sentence::Binary(op, s.make_box(), p.make_box()))
}
pub fn or(s: impl Makes<Sentence>, p: impl Makes<Sentence>) -> S {
    binary(BinaryOp::Disjunction, s, p)
}
pub fn and(s: impl Makes<Sentence>, p: impl Makes<Sentence>) -> S {
    binary(BinaryOp::Conjunction, s, p)
}
pub fn implies(s: impl Makes<Sentence>, p: impl Makes<Sentence>) -> S {
    binary(BinaryOp::Implication, s, p)
}
pub fn equiv(s: impl Makes<Sentence>, p: impl Makes<Sentence>) -> S {
    binary(BinaryOp::Equivalence, s, p)
}

pub fn assumed(t: impl Makes<Sentence>) -> P {
    Box::new(Proof::new(t.make(), Rule::Assumption(false)))
}
pub fn assumed_dis(t: impl Makes<Sentence>) -> P {
    Box::new(Proof::new(t.make(), Rule::Assumption(true)))
}
pub fn and_intro(t: impl Makes<Sentence>, s: impl Makes<Proof>, r: impl Makes<Proof>) -> P {
    Box::new(Proof::new(
        t.make(),
        Rule::ConjunctionIntro(s.make_box(), r.make_box()),
    ))
}
pub fn and_elim(t: impl Makes<Sentence>, s: impl Makes<Proof>) -> P {
    Box::new(Proof::new(t.make(), Rule::ConjunctionElim(s.make_box())))
}
pub fn implies_intro(t: impl Makes<Sentence>, s: impl Makes<Proof>) -> P {
    Box::new(Proof::new(t.make(), Rule::ImplicationIntro(s.make_box())))
}
pub fn implies_elim(t: impl Makes<Sentence>, s: impl Makes<Proof>, r: impl Makes<Proof>) -> P {
    Box::new(Proof::new(
        t.make(),
        Rule::ImplicationElim(s.make_box(), r.make_box()),
    ))
}
pub fn or_intro(t: impl Makes<Sentence>, s: impl Makes<Proof>) -> P {
    Box::new(Proof::new(t.make(), Rule::DisjunctionIntro(s.make_box())))
}
pub fn or_elim(
    t: impl Makes<Sentence>,
    s: impl Makes<Proof>,
    r: impl Makes<Proof>,
    q: impl Makes<Proof>,
) -> P {
    Box::new(Proof::new(
        t.make(),
        Rule::DisjunctionElim(s.make_box(), r.make_box(), q.make_box()),
    ))
}
pub fn neg_intro(t: impl Makes<Sentence>, s: impl Makes<Proof>, r: impl Makes<Proof>) -> P {
    Box::new(Proof::new(
        t.make(),
        Rule::NegationIntro(s.make_box(), r.make_box()),
    ))
}
pub fn neg_elim(t: impl Makes<Sentence>, s: impl Makes<Proof>, r: impl Makes<Proof>) -> P {
    Box::new(Proof::new(
        t.make(),
        Rule::NegationElim(s.make_box(), r.make_box()),
    ))
}
pub fn equiv_intro(t: impl Makes<Sentence>, s: impl Makes<Proof>, r: impl Makes<Proof>) -> P {
    Box::new(Proof::new(
        t.make(),
        Rule::EquivalenceIntro(s.make_box(), r.make_box()),
    ))
}
pub fn equiv_elim(t: impl Makes<Sentence>, s: impl Makes<Proof>) -> P {
    Box::new(Proof::new(t.make(), Rule::EquivalenceElim(s.make_box())))
}
