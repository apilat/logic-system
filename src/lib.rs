use std::{
    collections::{HashMap, HashSet},
    fmt::{self, Display, Formatter},
    str::FromStr,
};
use BinaryOp::*;
use UnaryOp::*;

pub mod natural_deduction;
mod parser;
pub mod tableaux;
mod fast;

type Letter = (char, u32);

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub enum UnaryOp {
    Negation,
}

impl UnaryOp {
    pub fn symbol(&self) -> char {
        match self {
            Negation => '¬',
        }
    }

    pub fn apply(&self, x: bool) -> bool {
        match self {
            Negation => !x,
        }
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub enum BinaryOp {
    Conjunction,
    Disjunction,
    Implication,
    Equivalence,
}

impl BinaryOp {
    pub fn symbol(&self) -> char {
        match self {
            Conjunction => '∧',
            Disjunction => '∨',
            Implication => '→',
            Equivalence => '⟷',
        }
    }

    pub fn apply(&self, a: bool, b: bool) -> bool {
        match self {
            Conjunction => a && b,
            Disjunction => a || b,
            Implication => !a || b,
            Equivalence => a == b,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Sentence {
    Letter(Letter),
    Unary(UnaryOp, Box<Sentence>),
    Binary(BinaryOp, Box<Sentence>, Box<Sentence>),
}

#[derive(Debug, Clone)]
pub struct Structure {
    assignment: HashMap<Letter, bool>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct TruthTable {
    vars: Vec<Letter>,
    evaluated: Vec<bool>,
}

impl Display for Sentence {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use Sentence::*;
        match self {
            Letter((c, 0)) => write!(f, "{}", c),
            Letter((c, i)) => write!(f, "{}_{}", c, i),
            Unary(op, x) => write!(f, "{}{}", op.symbol(), x),
            Binary(op, a, b) => write!(f, "({} {} {})", a, op.symbol(), b),
        }
    }
}

impl FromStr for Sentence {
    type Err = parser::Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        parser::parse(s)
    }
}

#[derive(Debug, Clone)]
// TODO derive(Error) with thiserror
pub struct MissingLetter((char, u32));

impl Sentence {
    pub fn evaluate(&self, structure: &Structure) -> Result<bool, MissingLetter> {
        use Sentence::*;
        Ok(match self {
            &Letter((c, i)) => structure
                .assignment
                .get(&(c, i))
                .copied()
                .ok_or(MissingLetter((c, i)))?,
            Unary(op, x) => op.apply(x.evaluate(structure)?),
            Binary(op, a, b) => op.apply(a.evaluate(structure)?, b.evaluate(structure)?),
        })
    }

    fn find_variables(&self, vars: &mut HashSet<Letter>) {
        use Sentence::*;
        match self {
            Letter(x) => {
                vars.insert(*x);
            }
            Unary(_, x) => {
                x.find_variables(vars);
            }
            Binary(_, a, b) => {
                a.find_variables(vars);
                b.find_variables(vars);
            }
        }
    }

    pub fn truth_table(&self) -> TruthTable {
        let mut vars = HashSet::new();
        self.find_variables(&mut vars);
        let mut vars: Vec<Letter> = vars.into_iter().collect();
        // Sort variables to ensure consistent ordering and allow correct comparison
        vars.sort_unstable();

        assert!(
            vars.len() < 32,
            "Cannot create a truth table 32 or more variables."
        );

        let mut evaluated = Vec::new();
        for mask in 0..1 << vars.len() {
            let mut structure = Structure::new();
            for (i, var) in vars.iter().enumerate() {
                structure.set(*var, mask & (1 << i) > 0);
            }
            let value = self
                .evaluate(&structure)
                .expect("Evaluation failed despite having all variables");
            evaluated.push(value);
        }

        TruthTable { vars, evaluated }
    }
}

impl Display for Structure {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut first = true;
        for (&letter, &val) in self.assignment.iter() {
            if !first {
                write!(f, ", ")?;
            } else {
                first = false;
            }

            // TODO Newtype Letter and have direct Display implementation
            write!(
                f,
                "{}",
                if val {
                    Sentence::Letter(letter)
                } else {
                    Sentence::Unary(Negation, Box::new(Sentence::Letter(letter)))
                }
            )?;
        }
        Ok(())
    }
}

impl Structure {
    pub fn new() -> Structure {
        Structure {
            assignment: HashMap::new(),
        }
    }

    pub fn set(&mut self, var: Letter, value: bool) {
        self.assignment.insert(var, value);
    }

    pub fn get(&self, var: Letter) -> Option<bool> {
        self.assignment.get(&var).copied()
    }
}

impl TruthTable {
    pub fn is_tautology(&self) -> bool {
        self.evaluated.iter().all(|x| *x)
    }

    pub fn is_contradiction(&self) -> bool {
        self.evaluated.iter().all(|x| !x)
    }

    pub fn get_possible_structure(&self, value: bool) -> Option<Structure> {
        self.evaluated
            .iter()
            .enumerate()
            .find(|(_, x)| **x == value)
            .map(|(mask, _)| {
                let mut structure = Structure::new();
                for (i, var) in self.vars.iter().enumerate() {
                    structure.set(*var, mask & (1 << i) > 0);
                }
                structure
            })
    }
}

#[cfg(test)]
mod tests {
    use super::{Sentence::*, *};

    #[test]
    fn display() {
        assert_eq!(format!("{}", Letter(('a', 3))), "a_3");
        assert_eq!(format!("{}", Letter(('γ', 0))), "γ");
        assert_eq!(
            format!("{}", Unary(Negation, Box::new(Letter(('θ', 1))))),
            "¬θ_1"
        );
        assert_eq!(
            format!(
                "{}",
                Binary(
                    Conjunction,
                    Box::new(Letter(('a', 2))),
                    Box::new(Letter(('b', 3)))
                )
            ),
            "(a_2 ∧ b_3)"
        );
        assert_eq!(
            format!(
                "{}",
                Binary(
                    Disjunction,
                    Box::new(Letter(('c', 12))),
                    Box::new(Letter(('x', 0)))
                )
            ),
            "(c_12 ∨ x)"
        );
        assert_eq!(
            format!(
                "{}",
                Binary(
                    Implication,
                    Box::new(Letter(('e', 5))),
                    Box::new(Letter(('ϵ', 0)))
                )
            ),
            "(e_5 → ϵ)"
        );
        assert_eq!(
            format!(
                "{}",
                Binary(
                    Equivalence,
                    Box::new(Letter(('Ι', 0))),
                    Box::new(Letter(('Ϻ', 10)))
                )
            ),
            "(Ι ⟷ Ϻ_10)"
        );

        assert_eq!(
            format!(
                "{}",
                Binary(
                    Implication,
                    Box::new(Binary(
                        Disjunction,
                        Box::new(Letter(('A', 0))),
                        Box::new(Letter(('B', 0)))
                    )),
                    Box::new(Binary(
                        Conjunction,
                        Box::new(Letter(('C', 1))),
                        Box::new(Unary(
                            Negation,
                            Box::new(Binary(
                                Conjunction,
                                Box::new(Letter(('B', 0))),
                                Box::new(Letter(('D', 0)))
                            ))
                        ))
                    ))
                )
            ),
            "((A ∨ B) → (C_1 ∧ ¬(B ∧ D)))"
        );
    }

    #[test]
    fn evaluate() {
        let structure = Structure {
            assignment: HashMap::from([(('T', 0), true), (('F', 0), false)]),
        };
        let t = Letter(('T', 0));
        let f = Letter(('F', 0));
        let u = Letter(('U', 0));

        assert!(matches!(t.evaluate(&structure), Ok(true)));
        assert!(matches!(f.evaluate(&structure), Ok(false)));
        assert!(matches!(
            u.evaluate(&structure),
            Err(MissingLetter(('U', 0)))
        ));

        for (op, table) in [
            (Conjunction, [false, false, false, true]),
            (Disjunction, [false, true, true, true]),
            (Implication, [true, true, false, true]),
            (Equivalence, [true, false, false, true]),
        ] {
            let evaluate_with = |a: &Sentence, b: &Sentence| {
                Binary(op, Box::new(a.clone()), Box::new(b.clone())).evaluate(&structure)
            };

            assert_eq!(evaluate_with(&f, &f).unwrap(), table[0]);
            assert_eq!(evaluate_with(&f, &t).unwrap(), table[1]);
            assert_eq!(evaluate_with(&t, &f).unwrap(), table[2]);
            assert_eq!(evaluate_with(&t, &t).unwrap(), table[3]);
            // Both need to be u or we might fail in some cases due to lazy evaluation
            assert!(matches!(
                evaluate_with(&u, &u),
                Err(MissingLetter(('U', 0)))
            ));
        }
    }

    #[test]
    fn truth_table() {
        // P -> P v Q
        assert_eq!(
            Binary(
                Implication,
                Box::new(Letter(('P', 0))),
                Box::new(Binary(
                    Disjunction,
                    Box::new(Letter(('P', 0))),
                    Box::new(Letter(('Q', 0))),
                )),
            )
            .truth_table(),
            TruthTable {
                vars: vec![('P', 0), ('Q', 0)],
                evaluated: vec![true, true, true, true]
            }
        );

        // P ^ Q <-> R
        assert_eq!(
            Binary(
                Equivalence,
                Box::new(Binary(
                    Conjunction,
                    Box::new(Letter(('P', 0))),
                    Box::new(Letter(('Q', 0)))
                )),
                Box::new(Letter(('R', 0)))
            )
            .truth_table(),
            TruthTable {
                vars: vec![('P', 0), ('Q', 0), ('R', 0)],
                evaluated: vec![true, true, true, false, false, false, false, true]
            }
        );

        // R -> S ^ ~S
        assert_eq!(
            Binary(
                Implication,
                Box::new(Letter(('R', 0))),
                Box::new(Binary(
                    Conjunction,
                    Box::new(Letter(('S', 0))),
                    Box::new(Unary(Negation, Box::new(Letter(('S', 0)))))
                ))
            )
            .truth_table(),
            TruthTable {
                vars: vec![('R', 0), ('S', 0)],
                evaluated: vec![true, false, true, false]
            }
        )
    }
}
