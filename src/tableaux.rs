use crate::{fast::*, natural_deduction::Proof, BinaryOp, Sentence, UnaryOp};
use std::collections::{HashMap, VecDeque};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Rule {
    Assumption,
    Conjunction,
    NegConjunction,
    Disjunction,
    NegDisjunction1,
    NegDisjunction2,
    Implication,
    NegImplication1,
    NegImplication2,
    Equivalence,
    NegEquivalence1,
    NegEquivalence2,
    DoubleNegElim,
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct State {
    // TODO Investigate whether the ends always form a suffix.
    // Each end stores a list of directions (L/R) at each branch which allows splitting the ends at
    // every branch.
    ends: Vec<(usize, VecDeque<bool>)>,
    known_true: HashMap<Sentence, usize>,
    known_false: HashMap<Sentence, usize>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Deduction {
    source: usize,
    rule: Rule,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Link {
    // flip is `false` if `other = neg(this)` and `true` if `neg(other) = this`
    Contradiction { other: usize, flip: bool },
    Straight(usize),
    Branch(usize, usize),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Tableaux {
    stmts: Vec<Sentence>,
    links: Vec<Option<Link>>,
    backlinks: Vec<Deduction>,
}

impl State {
    fn new() -> Self {
        State {
            ends: vec![(0, VecDeque::new())],
            known_true: HashMap::new(),
            known_false: HashMap::new(),
        }
    }

    fn assume(&mut self, stmt: &Sentence, n: usize) -> (Option<(usize, bool)>, bool) {
        let (contradiction, already_had);
        if let Sentence::Unary(UnaryOp::Negation, neg_stmt) = stmt {
            contradiction = self.known_true.get(neg_stmt).map(|x| (*x, false));
            already_had = self.known_false.contains_key(neg_stmt);
            if !already_had {
                self.known_false.insert(*neg_stmt.clone(), n);
            }
        } else {
            contradiction = self.known_false.get(stmt).map(|x| (*x, true));
            // If already exists we do not want to overwrite `n` as it might interfere with
            // branching later.
            already_had = self.known_true.contains_key(stmt);
            if !already_had {
                self.known_true.insert(stmt.clone(), n);
            }
        }
        (contradiction, already_had)
    }

    fn unassume(&mut self, stmt: &Sentence) {
        if let Sentence::Unary(UnaryOp::Negation, neg_stmt) = stmt {
            self.known_false
                .remove(neg_stmt)
                .expect("unassuming non-existent assumption");
        } else {
            self.known_true
                .remove(stmt)
                .expect("unassuming non-existent assumption");
        }
    }

    // Splits ends in left (kept in self) and right (returned).
    fn split_ends(&mut self) -> Vec<(usize, VecDeque<bool>)> {
        let mut right_ends = Vec::new();
        for (end, mut dirs) in std::mem::take(&mut self.ends) {
            if dirs.pop_front().expect("end has no direction during split") {
                right_ends.push((end, dirs));
            } else {
                self.ends.push((end, dirs));
            }
        }
        right_ends
    }
}

impl Deduction {
    fn new(source: usize, rule: Rule) -> Deduction {
        Deduction { source, rule }
    }

    fn assumption() -> Deduction {
        Deduction {
            source: usize::MAX,
            rule: Rule::Assumption,
        }
    }
}

impl Tableaux {
    pub fn prove(stmt: Sentence) -> Result<Tableaux, ()> {
        let mut tableaux = Tableaux {
            stmts: vec![Sentence::Unary(UnaryOp::Negation, Box::new(stmt))],
            links: vec![None],
            backlinks: vec![Deduction::assumption()],
        };
        tableaux.go(0, &mut State::new());
        if tableaux.is_closed() {
            Ok(tableaux)
        } else {
            Err(())
        }
    }

    fn go(&mut self, n: usize, state: &mut State) {
        let (contradiction, already_had) = state.assume(&self.stmts[n], n);
        if let Some((other, flip)) = contradiction {
            self.links[n] = Some(Link::Contradiction { other, flip });
        } else {
            match &self.stmts[n] {
                Sentence::Letter(_) => (),

                Sentence::Binary(BinaryOp::Conjunction, a, b) => {
                    // Need to clone here since I can't convince compiler we are only accessing
                    // disjoint parts of stmts.
                    let (a, b) = (a.clone(), b.clone());
                    self.append(&a, Deduction::new(n, Rule::Conjunction), state);
                    self.append(&b, Deduction::new(n, Rule::Conjunction), state);
                }

                Sentence::Binary(BinaryOp::Disjunction, a, b) => {
                    let (a, b) = (a.clone(), b.clone());
                    self.split(&a, &b, Deduction::new(n, Rule::Disjunction), state);
                }

                Sentence::Binary(BinaryOp::Implication, a, b) => {
                    let (a, b) = (a.clone(), b.clone());
                    self.append(&*or(neg(a), b), Deduction::new(n, Rule::Implication), state);
                }

                Sentence::Binary(BinaryOp::Equivalence, a, b) => {
                    let (a, b) = (a.clone(), b.clone());
                    self.append(
                        &*implies(&a, &b),
                        Deduction::new(n, Rule::Equivalence),
                        state,
                    );
                    self.append(&*implies(b, a), Deduction::new(n, Rule::Equivalence), state);
                }

                Sentence::Unary(UnaryOp::Negation, neg_stmt) => match &**neg_stmt {
                    Sentence::Letter(_) => (),

                    Sentence::Unary(UnaryOp::Negation, dneg_stmt) => {
                        let dneg_stmt = dneg_stmt.clone();
                        self.append(&*dneg_stmt, Deduction::new(n, Rule::DoubleNegElim), state);
                    }

                    Sentence::Binary(BinaryOp::Conjunction, a, b) => {
                        let (a, b) = (a.clone(), b.clone());
                        self.append(
                            &*or(neg(a), neg(b)),
                            Deduction::new(n, Rule::NegConjunction),
                            state,
                        );
                    }

                    Sentence::Binary(BinaryOp::Disjunction, a, b) => {
                        let (a, b) = (a.clone(), b.clone());
                        self.append(&*neg(a), Deduction::new(n, Rule::NegDisjunction1), state);
                        self.append(&*neg(b), Deduction::new(n, Rule::NegDisjunction2), state);
                    }

                    Sentence::Binary(BinaryOp::Implication, a, b) => {
                        let (a, b) = (a.clone(), b.clone());
                        self.append(&*a, Deduction::new(n, Rule::NegImplication1), state);
                        self.append(&*neg(b), Deduction::new(n, Rule::NegImplication2), state);
                    }

                    Sentence::Binary(BinaryOp::Equivalence, a, b) => {
                        let (a, b) = (a.clone(), b.clone());
                        self.append(
                            &*or(&a, &b),
                            Deduction::new(n, Rule::NegEquivalence1),
                            state,
                        );
                        self.append(
                            &*or(neg(a), neg(b)),
                            Deduction::new(n, Rule::NegEquivalence2),
                            state,
                        );
                    }
                },
            }
        }

        if let Some(link) = self.links[n] {
            match link {
                Link::Contradiction { .. } => (),
                Link::Straight(x) => self.go(x, state),
                Link::Branch(x, y) => {
                    let left_ends = state.split_ends();
                    self.go(x, state);
                    state.ends = left_ends;
                    self.go(y, state);
                }
            }
        }

        if !already_had {
            state.unassume(&self.stmts[n]);
        }
    }

    fn append(&mut self, stmt: &Sentence, deduction: Deduction, state: &mut State) {
        let mut ends = Vec::with_capacity(state.ends.len());
        for (end, dirs) in std::mem::take(&mut state.ends) {
            let n = self.stmts.len();
            self.stmts.push(stmt.clone());
            self.links.push(None);
            self.backlinks.push(deduction);
            // We might have already classified this end as a contradiction in which case simply
            // ignore it.
            if self.links[end].is_none() {
                self.links[end] = Some(Link::Straight(n));
                ends.push((n, dirs));
            }
        }
        state.ends = ends;
    }

    fn split(
        &mut self,
        left: &Sentence,
        right: &Sentence,
        deduction: Deduction,
        state: &mut State,
    ) {
        let mut ends = Vec::with_capacity(state.ends.len() * 2);
        for (end, mut dirs) in std::mem::take(&mut state.ends) {
            let n = self.stmts.len();
            self.stmts.push(left.clone());
            self.links.push(None);
            self.backlinks.push(deduction);
            self.stmts.push(right.clone());
            self.links.push(None);
            self.backlinks.push(deduction);
            if self.links[end].is_none() {
                self.links[end] = Some(Link::Branch(n, n + 1));
                let mut left_dirs = dirs.clone();
                left_dirs.push_back(false);
                dirs.push_back(true);
                ends.push((n, left_dirs));
                ends.push((n + 1, dirs));
            }
        }
        state.ends = ends;
    }

    fn is_closed(&self) -> bool {
        let mut current = vec![0];
        while !current.is_empty() {
            let mut next = vec![];
            for x in current {
                match self.links[x] {
                    None => return false,
                    Some(Link::Contradiction { .. }) => (),
                    Some(Link::Straight(x)) => next.push(x),
                    Some(Link::Branch(x, y)) => {
                        next.push(x);
                        next.push(y);
                    }
                }
            }
            current = next;
        }
        true
    }

    pub fn natural_deduction(&self) -> Proof {
        if let Sentence::Unary(UnaryOp::Negation, target) = &self.stmts[0] {
            *neg_elim(
                target,
                self.go_proof(0, (&**target).clone()),
                assumed_dis(&self.stmts[0]),
            )
        } else {
            unreachable!()
        }
    }

    // Generates a proof of `target` by using contradictions and disjunctions, assuming all of the
    // statements in the tableaux are true.
    fn go_proof(&self, n: usize, target: Sentence) -> Proof {
        match self.links[n].expect("proof generation reached unclosed branch of tableaux") {
            Link::Contradiction { other, flip } => {
                let (n, m) = if flip { (n, other) } else { (other, n) };
                *neg_elim(target, self.reconstruct_proof(n), self.reconstruct_proof(m))
            }
            Link::Straight(x) => self.go_proof(x, target),
            Link::Branch(x, y) => *or_elim(
                target.clone(),
                self.reconstruct_proof(self.backlinks[x].source),
                self.go_proof(x, target.clone()),
                self.go_proof(y, target),
            ),
        }
    }

    // Reconstructs a proof of the `n`th statement in the tableaux by following backlinks.
    fn reconstruct_proof(&self, n: usize) -> Proof {
        let error = "incorrect use of deduction rule";
        let stmt = &self.stmts[n];
        let src = self.backlinks[n].source;

        match self.backlinks[n].rule {
            Rule::Assumption | Rule::Disjunction => *assumed_dis(stmt),

            Rule::Conjunction => *and_elim(stmt, self.reconstruct_proof(src)),

            Rule::NegConjunction => {
                let (a, b) = if let Sentence::Unary(UnaryOp::Negation, neg_stmt) = &self.stmts[src]
                {
                    if let Sentence::Binary(BinaryOp::Conjunction, a, b) = &**neg_stmt {
                        (a, b)
                    } else {
                        unreachable!(error)
                    }
                } else {
                    unreachable!(error)
                };

                *neg_elim(
                    stmt,
                    or_intro(
                        stmt,
                        neg_intro(
                            neg(b),
                            or_intro(
                                stmt,
                                neg_intro(
                                    neg(a),
                                    and_intro(and(a, b), assumed_dis(a), assumed_dis(b)),
                                    self.reconstruct_proof(src),
                                ),
                            ),
                            assumed_dis(neg(stmt)),
                        ),
                    ),
                    assumed_dis(neg(stmt)),
                )
            }

            Rule::NegDisjunction1 => {
                let (a, b) = if let Sentence::Unary(UnaryOp::Negation, neg_stmt) = &self.stmts[src]
                {
                    if let Sentence::Binary(BinaryOp::Disjunction, a, b) = &**neg_stmt {
                        (a, b)
                    } else {
                        unreachable!(error)
                    }
                } else {
                    unreachable!(error)
                };

                *neg_intro(
                    stmt,
                    or_intro(or(a, b), assumed_dis(a)),
                    self.reconstruct_proof(src),
                )
            }

            Rule::NegDisjunction2 => {
                let (a, b) = if let Sentence::Unary(UnaryOp::Negation, neg_stmt) = &self.stmts[src]
                {
                    if let Sentence::Binary(BinaryOp::Disjunction, a, b) = &**neg_stmt {
                        (a, b)
                    } else {
                        unreachable!(error)
                    }
                } else {
                    unreachable!(error)
                };

                *neg_intro(
                    stmt,
                    or_intro(or(a, b), assumed_dis(b)),
                    self.reconstruct_proof(src),
                )
            }

            Rule::Implication => {
                let (a, b) = if let Sentence::Binary(BinaryOp::Implication, a, b) = &self.stmts[src]
                {
                    (a, b)
                } else {
                    unreachable!(error)
                };

                *neg_elim(
                    stmt,
                    or_intro(
                        stmt,
                        neg_intro(
                            neg(a),
                            or_intro(
                                stmt,
                                implies_elim(b, assumed_dis(a), self.reconstruct_proof(src)),
                            ),
                            assumed_dis(neg(stmt)),
                        ),
                    ),
                    assumed_dis(neg(stmt)),
                )
            }

            Rule::NegImplication1 => {
                let (a, b) = if let Sentence::Unary(UnaryOp::Negation, neg_stmt) = &self.stmts[src]
                {
                    if let Sentence::Binary(BinaryOp::Implication, a, b) = &**neg_stmt {
                        (a, b)
                    } else {
                        unreachable!(error)
                    }
                } else {
                    unreachable!(error)
                };

                *neg_elim(
                    a,
                    implies_intro(
                        implies(a, b),
                        neg_elim(b, assumed_dis(a), assumed_dis(neg(a))),
                    ),
                    self.reconstruct_proof(src),
                )
            }

            Rule::NegImplication2 => {
                let (a, b) = if let Sentence::Unary(UnaryOp::Negation, neg_stmt) = &self.stmts[src]
                {
                    if let Sentence::Binary(BinaryOp::Implication, a, b) = &**neg_stmt {
                        (a, b)
                    } else {
                        unreachable!(error)
                    }
                } else {
                    unreachable!(error)
                };

                *neg_intro(
                    neg(b),
                    implies_intro(implies(a, b), assumed_dis(b)),
                    self.reconstruct_proof(src),
                )
            }

            Rule::Equivalence => *equiv_elim(stmt, self.reconstruct_proof(src)),

            Rule::NegEquivalence1 => {
                let (a, b) = if let Sentence::Unary(UnaryOp::Negation, neg_stmt) = &self.stmts[src]
                {
                    if let Sentence::Binary(BinaryOp::Equivalence, a, b) = &**neg_stmt {
                        (a, b)
                    } else {
                        unreachable!(error)
                    }
                } else {
                    unreachable!(error)
                };

                *neg_elim(
                    stmt,
                    or_intro(
                        stmt,
                        neg_elim(
                            b,
                            or_intro(
                                stmt,
                                neg_elim(
                                    a,
                                    equiv_intro(
                                        equiv(a, b),
                                        implies_intro(
                                            implies(a, b),
                                            neg_elim(b, assumed_dis(a), assumed_dis(neg(a))),
                                        ),
                                        implies_intro(
                                            implies(b, a),
                                            neg_elim(a, assumed_dis(b), assumed_dis(neg(b))),
                                        ),
                                    ),
                                    self.reconstruct_proof(src),
                                ),
                            ),
                            assumed_dis(neg(stmt)),
                        ),
                    ),
                    assumed_dis(neg(stmt)),
                )
            }

            Rule::NegEquivalence2 => {
                let (a, b) = if let Sentence::Unary(UnaryOp::Negation, neg_stmt) = &self.stmts[src]
                {
                    if let Sentence::Binary(BinaryOp::Equivalence, a, b) = &**neg_stmt {
                        (a, b)
                    } else {
                        unreachable!(error)
                    }
                } else {
                    unreachable!(error)
                };

                *neg_elim(
                    stmt,
                    or_intro(
                        stmt,
                        neg_intro(
                            neg(b),
                            or_intro(
                                stmt,
                                neg_intro(
                                    neg(a),
                                    equiv_intro(
                                        equiv(a, b),
                                        implies_intro(implies(a, b), assumed_dis(b)),
                                        implies_intro(implies(b, a), assumed_dis(a)),
                                    ),
                                    self.reconstruct_proof(src),
                                ),
                            ),
                            assumed_dis(neg(stmt)),
                        ),
                    ),
                    assumed_dis(neg(stmt)),
                )
            }

            Rule::DoubleNegElim => {
                *neg_elim(stmt, assumed_dis(neg(stmt)), self.reconstruct_proof(src))
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn implication_chain() {
        let p = letter0('P');
        let q = letter0('P');
        let r = letter0('P');
        let ptq = implies(&p, &q);
        let qtr = implies(&p, &r);
        let ptr = implies(&p, &r);
        let stmt = implies(and(&ptq, &qtr), &ptr);

        let tableaux = Tableaux::prove(*stmt).unwrap();
        let proof = tableaux.natural_deduction();
        assert!(proof.verify().is_ok());
    }

    #[test]
    fn standard_proofs() {
        let (p, q) = (letter0('P'), letter0('Q'));
        for (assumption, conclusion) in [
            (neg(neg(&p)), p.clone()),
            (neg(and(&p, &q)), or(neg(&p), neg(&q))),
            (neg(or(&p, &q)), and(neg(&p), neg(&q))),
            (implies(&p, &q), or(neg(&p), &q)),
            (neg(implies(&p, &q)), and(&p, neg(&q))),
            (equiv(&p, &q), and(or(&p, neg(&q)), or(neg(&p), &q))),
            (neg(equiv(&p, &q)), and(or(&p, &q), or(neg(&p), neg(&q)))),
        ] {
            let stmt = *implies(assumption, conclusion);
            let tableaux = Tableaux::prove(stmt).unwrap();
            for i in 0..tableaux.stmts.len() {
                println!(
                    "{} {} to {:?} from {:?}",
                    i, tableaux.stmts[i], tableaux.links[i], tableaux.backlinks[i]
                );
            }
            let proof = tableaux.natural_deduction();
            println!("{}", proof);
            if let Err(small) = proof.verify() {
                println!("{:?}\n{}", small, small);
            }
            assert!(proof.verify().is_ok());
        }
    }
}
