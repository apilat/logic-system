use crate::{BinaryOp, Sentence, UnaryOp};
use std::{iter::Peekable, vec::IntoIter};

#[derive(Debug)]
pub enum Error {
    UnrecognizedIdent(String),
    UnexpectedEOF,
    UnexpectedToken(Token, String),
    TrailingToken(Token),
}

pub fn parse(s: &str) -> Result<Sentence, Error> {
    let mut tokens = tokenize(s)?.into_iter().peekable();
    let sentence = build_binary(&mut tokens)?;
    if let Some(tok) = tokens.next() {
        Err(Error::TrailingToken(tok))
    } else {
        Ok(sentence)
    }
}

#[derive(Debug)]
pub enum Token {
    Letter(char),
    Paren(bool),
    UnaryOp(UnaryOp),
    BinaryOp(BinaryOp),
}

fn tokenize(s: &str) -> Result<Vec<Token>, Error> {
    let mut tokens = Vec::new();
    let mut iter = s.chars().peekable();

    while let Some(c) = iter.next() {
        match c {
            _ if c.is_whitespace() => (),

            '(' | '[' | '{' => tokens.push(Token::Paren(true)),
            ')' | ']' | '}' => tokens.push(Token::Paren(false)),

            _ => {
                let symbolic = !c.is_alphabetic();
                let mut ident = String::from(c);
                let mut single = Some(c);

                while let Some(c) = iter.peek() {
                    let valid = if symbolic {
                        ['-', '>'].contains(c)
                    } else {
                        c.is_alphabetic()
                    };

                    if valid {
                        ident.push(iter.next().unwrap());
                        single = None;
                    } else {
                        break;
                    }
                }

                tokens.push(match ident.as_str() {
                    "~" | "not" => Token::UnaryOp(UnaryOp::Negation),
                    "^" | "and" => Token::BinaryOp(BinaryOp::Conjunction),
                    "v" | "or" => Token::BinaryOp(BinaryOp::Disjunction),
                    "->" | "implies" => Token::BinaryOp(BinaryOp::Implication),
                    "<>" | "<->" | "equiv" => Token::BinaryOp(BinaryOp::Equivalence),
                    _ => {
                        if let Some(c) = single {
                            Token::Letter(c)
                        } else {
                            return Err(Error::UnrecognizedIdent(ident));
                        }
                    }
                });
            }
        }
    }

    Ok(tokens)
}

fn build_binary(tokens: &mut Peekable<IntoIter<Token>>) -> Result<Sentence, Error> {
    let lhs = build_unary(tokens)?;
    if let Some(tok) = tokens.peek() {
        match tok {
            Token::BinaryOp(_) => {
                let op = if let Some(Token::BinaryOp(op)) = tokens.next() {
                    op
                } else {
                    unreachable!()
                };
                let rhs = build_unary(tokens)?;
                Ok(Sentence::Binary(op, Box::new(lhs), Box::new(rhs)))
            }

            _ => Ok(lhs),
        }
    } else {
        Ok(lhs)
    }
}

fn build_unary(tokens: &mut Peekable<IntoIter<Token>>) -> Result<Sentence, Error> {
    let op = if let Some(Token::UnaryOp(_)) = tokens.peek() {
        if let Some(Token::UnaryOp(op)) = tokens.next() {
            Some(op)
        } else {
            unreachable!();
        }
    } else {
        None
    };

    let rhs = build_basic(tokens)?;
    if let Some(op) = op {
        Ok(Sentence::Unary(op, Box::new(rhs)))
    } else {
        Ok(rhs)
    }
}

fn build_basic(tokens: &mut Peekable<IntoIter<Token>>) -> Result<Sentence, Error> {
    if let Some(tok) = tokens.next() {
        match tok {
            Token::Letter(x) => Ok(Sentence::Letter((x, 0))),

            Token::Paren(true) => {
                let sentence = build_binary(tokens)?;
                match tokens.next().ok_or(Error::UnexpectedEOF)? {
                    Token::Paren(false) => Ok(sentence),
                    tok => Err(Error::UnexpectedToken(tok, "closing paren".into())),
                }
            }

            _ => Err(Error::UnexpectedToken(tok, "start of sentence".into())),
        }
    } else {
        Err(Error::UnexpectedEOF)
    }
}
